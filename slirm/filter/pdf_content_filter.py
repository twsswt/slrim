import os

import zlib

from binascii import b2a_hex

from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.layout import LTPage, LAParams, LTTextBoxHorizontal, LTTextLineHorizontal, LTFigure, LTImage
from pdfminer.converter import PDFPageAggregator

from .automatic import OneToOneFilter


class open_pdf_file(object):

    def __init__(self, file_path):
        self.file_path = file_path

    def __enter__(self):
        self.file = open(self.file_path, mode='rb')
        return PDFDocument(PDFParser(self.file))

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()


def get_pdf_page_layouts(pdf_document):

    result = list()

    rsrcmgr = PDFResourceManager()
    device = PDFPageAggregator(rsrcmgr, laparams=LAParams())
    interpreter = PDFPageInterpreter(rsrcmgr, device)

    pdf_pages = PDFPage.create_pages(pdf_document)

    for page in pdf_pages:
        interpreter.process_page(page)
        layout = list(device.get_result())
        result.append(layout)

    return result


def reconstruct_text_block_from_text_line_horizontal_elements(index, layout):
    page_text = ''

    element = layout[index]
    while index < len(layout) and isinstance(element, LTTextLineHorizontal):
        element = layout[index]
        page_text += (element.get_text()[0:-1])
        index += 1

    return index, page_text


def extract_pdf_document_text(pdf_document):
    pages_of_text_blocks = list()

    for layout in get_pdf_page_layouts(pdf_document):

        page_of_text_blocks = list()

        index = 0

        while index < len(layout):

            element = layout[index]

            if isinstance(element, LTTextBoxHorizontal):
                page_of_text_blocks.append(element.get_text())

            elif isinstance(element, LTTextLineHorizontal):
                index, page_text = reconstruct_text_block_from_text_line_horizontal_elements(index, layout)
                page_of_text_blocks.append(page_text)

            index += 1

        pages_of_text_blocks.append(page_of_text_blocks)

    return pages_of_text_blocks


class PDFTextContentFilter(OneToOneFilter):

    def __init__(self, source_pipe, text_content_condition=lambda pages: True):

        def accept_on_pdf_content(entry):

            if 'local_file_path' not in entry:
                return False

            local_file_path = entry['local_file_path']

            if not os.path.exists(local_file_path):
                return False

            with open_pdf_file(local_file_path) as pdf_document:
                pages = extract_pdf_document_text(pdf_document)
                return text_content_condition(pages)

        super().__init__(source_pipe, accept_on_pdf_content)


def extract_images_from_pdf(pdf_document, image_directory):

    if not os.path.exists(image_directory):
        os.mkdir(image_directory)

    for layout in get_pdf_page_layouts(pdf_document):
        for element in layout:
            if isinstance(element, LTFigure):
                for sub_element in element:
                    if isinstance(sub_element, LTImage):
                        save_image(sub_element, image_directory)


def get_first_four_bytes_of_stream_as_hex(raw_data_stream):
    return b2a_hex(raw_data_stream[0:4])


def determine_image_type (first_four_bytes_as_hex):

    result = None

    if first_four_bytes_as_hex.startswith(b'ffd8'):
        result = 'jpeg'
    elif first_four_bytes_as_hex == b'89504e47':
        result = 'png'
    elif first_four_bytes_as_hex == b'47494638':
        result = 'gif'
    elif first_four_bytes_as_hex.startswith(b'424d'):
        result = 'bmp'
    elif first_four_bytes_as_hex.startswith(b'4889ec97'):
        return 'png'
    return result


def save_image (lt_image, images_directory):
    if lt_image.stream:
        raw_image_data = lt_image.stream.get_rawdata()

        first_four_bytes_as_hex = get_first_four_bytes_of_stream_as_hex(raw_image_data)

        #if first_four_bytes_as_hex == b'4889ec97':
        #    raw_image_data = zlib.decompress(raw_image_data)
        #    first_four_bytes_as_hex = get_first_four_bytes_of_stream_as_hex(raw_image_data)

        file_ext = determine_image_type(first_four_bytes_as_hex)

        if file_ext:
            file_path = images_directory + '/' + lt_image.name + "." + file_ext
            with open(file_path, mode='wb') as output_file:
                output_file.write(raw_image_data)


class PDFImageExtractionFilter(object):

    def __init__(self, source_pipe):
        self.source_pipe = source_pipe

    def pull(self):
        source = self.source_pipe.pull()

        for entry in source.get_entry_list():

            local_file_path = entry.get('local_file_path', None)

            if local_file_path is None or not os.path.exists(local_file_path):
                continue

            with open_pdf_file(local_file_path) as pdf_document:
                extracted_image_directory = os.path.splitext(local_file_path)[0] + "_images"
                extract_images_from_pdf(pdf_document, extracted_image_directory)

        return source
