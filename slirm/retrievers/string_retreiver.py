from bibtexparser.bparser import BibTexParser


class StringRetriever(object):

    def __init__(self, bibtex_string):
        self.bibtex_string = bibtex_string

    def pull(self):
        bibtex_parser = BibTexParser(common_strings=True)
        return bibtex_parser.parse(self.bibtex_string)