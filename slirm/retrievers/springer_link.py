import json
import requests

from bibtexparser.bparser import BibDatabase


def parse_springer_link_json_type(springer_link_article):
    springer_link_type = springer_link_article['genre']
    print(springer_link_type)

    """
    if ieee_type in {'Journals', 'Early Access'}:
        return 'article'
    elif ieee_type == 'Conferences':
        if 'publication_title' in ieee_article:
            return 'inproceedings'
        else:
            return 'proceedings'
    elif ieee_type == 'Standards':
        return 'manual'
    else:
        print(ieee_article)
    """



def convert_springer_link_json_to_bibtex_db(json_string):

    result = BibDatabase()

    for springer_link_article in json.loads(json_string)['records']:
        del springer_link_article['abstract']
        print(springer_link_article)
        new_entry = dict()

        new_entry['ID'] = springer_link_article['identifier']

        new_entry['ENTRYTYPE'] = parse_springer_link_json_type(springer_link_article)

        # new_entry['abstract'] =springer_link_article['abstract']
        new_entry['title'] = springer_link_article['title']

        """
        if 'authors' in springer_link_article:
            new_entry['author'] = parse_ieee_json_authors(springer_link_article)

        new_entry['keywords'] = parse_ieee_json_keywords(springer_link_article)

        new_entry['url'] = springer_link_article['pdf_url']

        new_entry['article_number'] = springer_link_article['article_number']

        if new_entry['ENTRYTYPE'] == 'article':
            parse_extra_bibtex_article_fields(springer_link_article, new_entry)

        elif new_entry['ENTRYTYPE'] == 'inproceedings':
            new_entry['booktitle'] = springer_link_article['publication_title']

        article_date = parse_ieee_json_date(springer_link_article)
        new_entry['year'] = article_date.strftime("%Y")
        new_entry['month'] = article_date.strftime("%B")

        new_entry['pages'] = ("%s-%s") % (springer_link_article['start_page'], springer_link_article['end_page'])
        """

        result.get_entry_list().append(new_entry)


    return result


class SpringerLinkRetrieve(object):

    def __init__(self, queries, api_key):
        self.queries = queries
        self.api_key = api_key

        self.cookies = {}

    def pull(self):

        url_template = 'http://api.springernature.com/metadata/json?api_key=%s&q=sort:date %s'

        user_agents = [
            'Mozilla/5.0 (Windows NT 6.1; WOW64)',
            'AppleWebKit/537.36 (KHTML, like Gecko)',
            'Chrome/35.0.1916.114 Safari/537.36']

        headers = {'User-Agent': " ".join(user_agents)}

        for query in self.queries:
            url = url_template %(self.api_key, query)

            try:
                response = requests.get(url, cookies=self.cookies, headers=headers)
            except ConnectionError:
                # TODO Wrap connection error with a Pipeline Exception.
                return None

        return convert_springer_link_json_to_bibtex_db(response.text)