import unittest

from slirm.retrievers import StringRetriever
from slirm.filter import PDFTextContentFilter


class PDFContentFilterTestCase(unittest.TestCase):

    def setUp(self):

        def text_content_condition(pages):
            for page in pages:
                for text_block in page:
                    if 'internet' in text_block:
                        return True
            return False

        entries = \
            """
             @article{Finerman:1969:EN:356540.356541,
                 local_file_path = {pdfs/Finerman_1969_EN_356540.356541.pdf}
            } 
            @ARTICLE{6740844, 
                local_file_path = {pdfs/6740844.pdf}
            }
        """

        string_retriever = StringRetriever(entries)
        self.pdf_content_filter = PDFTextContentFilter(string_retriever, text_content_condition)

    def test_pdf_content_filter(self):

        result = self.pdf_content_filter.pull()
        self.assertEqual(1, len(result.get_entry_list()))


if __name__ == '__main__':
    unittest.main()
