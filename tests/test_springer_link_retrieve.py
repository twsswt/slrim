import unittest

from slirm.retrievers import SpringerLinkRetrieve


class IEEEXploreRetrieveTestCase(unittest.TestCase):

    def test_retrieve(self):

        query = '"software testing"'
        api_key = 'a6cb064309bb238a35a383a30f510748'
        springer_link_retrieve = SpringerLinkRetrieve([query], api_key)
        bibtex_database = springer_link_retrieve.pull()

        print(bibtex_database.get_entry_list()[0])

        self.assertGreater(len(bibtex_database.get_entry_list()), 1)


if __name__ == '__main__':
    unittest.main()
